package pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty;


import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.R;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db.ProjektContract.Projekt;
import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

public class ProjektyListAdapter extends CursorAdapter implements ListAdapter {
	
	public static class ViewHolder {

		public TextView tvDate;
		public TextView tvTitle;

	}

	public ProjektyListAdapter(Context context, Cursor c) {
		super(context, c, 0);
	}

	@Override
	public View newView(Context context, Cursor c, ViewGroup parent) {
		View view = View.inflate(context, R.layout.note_row, null);
		ViewHolder vh = new ViewHolder(); 
		vh.tvDate = (TextView) view.findViewById(R.id.tvDate);
		vh.tvTitle = (TextView) view.findViewById(R.id.tvTitle);
		vh.tvDate.setText(c.getString(c.getColumnIndex(Projekt.COLUMN_NAME_ODDATA)));
		vh.tvTitle.setText(c.getString(c.getColumnIndex(Projekt.COLUMN_NAME_NAZWA)));
		view.setTag(vh);
		return view;
	}

	@Override
	public void bindView(View view, Context context, Cursor c) {
		ViewHolder vh = (ViewHolder) view.getTag();
		vh.tvDate.setText(c.getString(c.getColumnIndex(Projekt.COLUMN_NAME_ODDATA)));
		vh.tvTitle.setText(c.getString(c.getColumnIndex(Projekt.COLUMN_NAME_NAZWA)));
		
	}

}
