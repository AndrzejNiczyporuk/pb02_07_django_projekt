package pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty;

import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.ProjektEditorFragment.ProjektEditorListener;
import android.app.Activity;
import android.app.ListFragment;
import android.database.Cursor;
import android.os.Bundle;
import android.app.LoaderManager;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Loader;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

/**
 * fragment wy�wietlaj�cy list� projektow.
 * 
 * 
 */
public class ProjektyListFragment extends ListFragment implements
		LoaderCallbacks<Cursor>
{
	
	private ListView lst;		
	private static final int LOADER_NOTE_LIST = 1;

	public interface NoteListListener {
		/**
		 * wywoływana na kliknięcie w wiersz notatki.
		 * @param id identyfikator bazodanowy notatki.
		 */
		
		void onListClick(long id);
		
		
		/**
		 * wywoływana na długie kliknięcie w wiersz notatki.
		 * @param id identyfikator bazodanowy notatki.
		 */
		void onListLongClick(long id);
		

	}

	private NoteListListener listener;


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		LoaderManager lm = getLoaderManager();
		lm.initLoader(LOADER_NOTE_LIST, null, this);

	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		// tu mo�na by u�y� standardowego CursorLoader'a, gdyby�my mieli
		// ContentProvider'a i pos�ugiwali si� Uri.
		// klasy NoteListLoadera
		return new ProjketyListLoader(getActivity().getApplicationContext());
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		// dodanie klasy NoteListAdapter
		ListAdapter adapter = new ProjektyListAdapter(getActivity()
				.getApplicationContext(), data);
		setListAdapter(adapter);
		
		//dodanie obsługi clicka na ListView 
		getListView().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
			listener.onListClick(id);
				
			}
			
		});
		//dodanie obsługi long clika
		getListView().setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				
			listener.onListLongClick(id);
			return true;
			}
		});
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		setListAdapter(null);
	}

	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.listener = (NoteListListener) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		this.listener = null;
	}



	

	
}
