package pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty;

/*loader do listy */

import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db.ProjektContract;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db.ProjektDBOpenHelper;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db.ProjektContract.Projekt;
import android.content.Context;
import android.content.CursorLoader;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.Loader.ForceLoadContentObserver;

public class ProjketyListLoader extends CursorLoader {

	private ProjektDBOpenHelper dbHelper;
	private ContentObserver contentObserver = new ForceLoadContentObserver();

	public ProjketyListLoader(Context context) {
		super(context);
		this.dbHelper = new ProjektDBOpenHelper(context);
	}
	@Override
	public void onCanceled(Cursor cursor) {
		super.onCanceled(cursor);
		dbHelper.close();
	}

	@Override
	protected void onReset() {
		super.onReset();
		dbHelper.close();
	}
	@Override
	public Cursor loadInBackground() {

		SQLiteDatabase db = dbHelper.getReadableDatabase();
		
		String[] columns = new String[] { Projekt._ID, Projekt.COLUMN_NAME_ODDATA,
				Projekt.COLUMN_NAME_NAZWA };

		String selection = null;
		String[] selectionArgs = null;
		String groupBy = null;
		String having = null;
		String orderBy = Projekt.COLUMN_NAME_ODDATA + " ASC , "
				+ Projekt.COLUMN_NAME_NAZWA + " ASC";
		Cursor cursor = db.query(Projekt.TABLE_NAME, columns, selection,
				selectionArgs, groupBy, having, orderBy);
		
			
		if (cursor != null) {
			cursor.getCount();
			cursor.registerContentObserver(contentObserver);
			cursor.setNotificationUri(getContext().getContentResolver(),
					ProjektContract.NOTE_URI);
		}
		return cursor;

	}
}
