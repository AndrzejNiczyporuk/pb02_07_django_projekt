package pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.sync;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import android.util.Log;
import com.google.gson.reflect.TypeToken;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.sync.WsClient;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.sync.JSONConverter;

/**
* abstrakcja metod webserwisu.
* 
* @author andrzej 
* 
*/
public class WsClient {
	private static final String TAG = WsClient.class.getName();
	
	public NotesResultWs downloadNotes(String spec)
			throws IOException {

		HttpURLConnection conn = null;
		InputStream is = null;
		try {
			URL downloadNotesUrl = new URL(spec);
			conn = (HttpURLConnection) downloadNotesUrl.openConnection();
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setReadTimeout(15000  /* 15 seconds */);
			conn.setConnectTimeout(10000 /* 10 seconds */);  
			conn.setRequestMethod("GET");
			conn.setDoInput(true);
		//	conn.connect();
			is = conn.getInputStream();
			NotesResultWs  fromJson = JSONConverter.getInstance().fromJson(is,
					new TypeToken<NotesResultWs>() {

					});
			return fromJson;

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			InputStream err = conn.getErrorStream();
			InputStreamReader reader = new InputStreamReader(err, "UTF-8");
			char[] buffer = new char[1000];
			int read = 0;
			StringBuilder sb = new StringBuilder();
			while ((read = reader.read(buffer)) != -1) {
				sb.append(new String(buffer, 0, read));
			}
			Log.e(TAG, "downloadProjket(): sb.toString(): " + sb.toString());
			throw e;
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		return null;
	}

}
