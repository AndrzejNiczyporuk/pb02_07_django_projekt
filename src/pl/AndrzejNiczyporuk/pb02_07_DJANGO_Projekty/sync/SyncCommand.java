package pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.sync;

import java.io.IOException;

//import pl.pawelzadykowicz.pb02_data.accounts.NotepadAuthenticator;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.ContentProvider;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.util.Log;

/**
 * bazowa klasa dla operacji, jaką może wykonac nasz SyncAdapter.
 * 
 * @author andrzej
 * 
 */
public abstract class SyncCommand {

	private static final String TAG = SyncCommand.class.getName();

	/**
	 * wywołanie operacji synchronizacji.
	 * 
	 * @param context
	 * @param syncResult
	 * @param providerClient
	 */
	//public final void invoke(Context context, Account account,
	public final void invoke(Context context, ContentProviderClient providerClient, SyncResult syncResult) throws IOException {
		int tryCount = 0;
		boolean success = false;
		do {
			++tryCount;
			// String token = am.blockingGetAuthToken(account,NotepadAuthenticator.AUTH_TOKEN_TYPE, true);
			ContentProvider provider = providerClient.getLocalContentProvider();
			//boolean authSuccessful = performSync(token, am, account, provider, syncResult);
			boolean authSuccessful = performSync(provider, syncResult);
			Log.d(TAG, "invoke() - próba numer: " + tryCount);
			if (!authSuccessful) {
				//am.invalidateAuthToken(NotepadAuthenticator.ACCOUNT_TYPE, token);
				success = false;
			} else {
				success = true;
			}
		} while (!success && tryCount < 2);
	}

	//protected abstract boolean performSync(String token, AccountManager am, Account account, ContentProvider provider, SyncResult syncResult);
	protected abstract boolean performSync( ContentProvider provider, SyncResult syncResult);

}
