package pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.sync;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db.ProjektContract.Projekt;
import android.content.ContentValues;

public class NoteWs {

	public String nazwa;
	public String opis;
	public String Od_data;
	public String Do_data;
	public String Link;
	public String Status;
//	public String  wykonawcy;
	public Long ID;
	
	


	public ContentValues asContentValues() {
		ContentValues ret = new ContentValues(5);
		ret.put(Projekt.COLUMN_NAME_NAZWA,  nazwa);
		ret.put(Projekt.COLUMN_NAME_OPIS, opis);
		ret.put(Projekt.COLUMN_NAME_ODDATA, Od_data);
		ret.put(Projekt.COLUMN_NAME_DODATA, Do_data);
		ret.put(Projekt.COLUMN_NAME_LINK, Link);
		ret.put(Projekt.COLUMN_NAME_STATUS, Status);
		ret.put(Projekt.COLUMN_NAME_SERVER_ID, ID);
		
	//	ret.put(Projekt.COLUMN_NAME_WYKONAWCY, wykonawcy);
		return ret;
	}
	
}
