package pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.sync.NoteWs;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.sync.NotesResultWs;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.sync.WsClient;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db.ProjektContract;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db.ProjektToDo;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db.ProjektDBOpenHelper;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db.ProjektContract.Projekt;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.sync.SyncCommand;

public class DownloadProjektCommand {

	private static final String TAG = DownloadProjektCommand.class.getName();
	private static WsClient wsClient = new WsClient();
	private static ContentResolver cr;

	public static boolean performSync(Context ctx) throws IOException {
		// TODO Auto-generated method stub
	//	Log.i(TAG, "performSync(); pobieranie danych JOSN");

		boolean ret = false;
		ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();
		cr = ctx.getContentResolver();
		NotesResultWs downloadNotesResult = wsClient.downloadNotes("http://192.168.139.1/jason/projekt-list/");
		// budujemy listę operacji insert i update dla projketów , które
		// już mamy w lokalnej bazie.
		Map<Long, NoteWs> downloadedNotesMap = new HashMap<Long, NoteWs>();
		List<NoteWs> notes = downloadNotesResult.Projekty;
		notes.remove(notes.size()-1); 
		for (NoteWs noteWs : notes) {
			downloadedNotesMap.put(noteWs.ID, noteWs);
			// Log.d(TAG, "performSync(); id: " + noteWs.ID + ", nazwa: " +
			// noteWs.nazwa);
			Log.d(TAG, "performSync() projekt: "+ JSONConverter.getInstance().toJson(noteWs));
		}
		// Log.d(TAG, "performSync() note: " +
		// JSONConverter.getInstance().toJson(noteWs));
		String[] projection = new String[] { Projekt._ID,
				Projekt.COLUMN_NAME_SERVER_ID };
		Cursor query = cr.query(ProjektContract.NOTE_URI, projection, null,
				null, null);
		while (query.moveToNext()) {
			long existingServerId = query.getLong(query
					.getColumnIndexOrThrow(Projekt.COLUMN_NAME_SERVER_ID));
			long existingNoteId = query.getLong(query
					.getColumnIndexOrThrow(Projekt._ID));
			NoteWs existingNoteWs = downloadedNotesMap.get(existingServerId);
			if (existingNoteWs!=null)
			{
			downloadedNotesMap.remove(existingServerId);
			Uri updateUri = ProjektContract.NOTE_URI.buildUpon()
					.appendPath(String.valueOf(existingNoteId)).build();
			Builder newUpdate = ContentProviderOperation.newUpdate(updateUri);
			newUpdate.withValues(existingNoteWs.asContentValues());
			operations.add(newUpdate.build());
			}
			else
			{
				//todo delete existingNoteId or put na serwer or nop 
			}
		}
		query.close();
		for (Map.Entry<Long, NoteWs> entry : downloadedNotesMap.entrySet()) {
			Builder newInsert = ContentProviderOperation
					.newInsert(ProjektContract.NOTE_URI);
			ContentValues values = entry.getValue().asContentValues();
			newInsert.withValues(values);

			operations.add(newInsert.build());
		}
		
		String strURI = ProjektContract.NOTE_URI.getEncodedAuthority();
		try {
			cr.applyBatch(strURI, operations);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OperationApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//cr.applyBatch(ProjektContract.NOTE_URI, operations);
		ret = true;
		// try {
		// TODO: aktualizacja bazy
		// provider.applyBatch(operations);
		// } catch (OperationApplicationException e) {
		// Log.e(TAG, "performSync()", e);
		// syncResult.databaseError = true;
		// ret = false;
		//
		// }
		return ret;
	}


}
