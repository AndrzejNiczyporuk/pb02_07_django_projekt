package pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.sync;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * obiekt serializujący i deserializujący obiekty na JSON'a.
 * 
 * @author zadykowiczp
 * 
 */
public final class JSONConverter {

	/**
	 * instancja konwertera.
	 */
	private static JSONConverter instance = new JSONConverter();
	/**
	 * delegat GSON obsługujący serializację.
	 */
	private Gson gson;

	/**
	 * konstruktor prywatny.
	 */
	private JSONConverter() {
		GsonBuilder builder = new GsonBuilder();
		// builder.serializeNulls();
		builder.setDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		if (!false) {
			builder.setPrettyPrinting(); // na produkcji już niepotrzebne - do
			// developerki łatwiej się to czyta.
		}
		gson = builder.create();
	}

	/**
	 * 
	 * @return instancja konwertera.
	 */
	public static JSONConverter getInstance() {
		return instance;
	}

	/**
	 * deserializacja do wybranego typu obiektu.
	 * 
	 * @param <T>
	 *            parametr typu docelowego obiektu
	 * @param json
	 *            reprezentacja JSON.
	 * @param classOfT
	 *            klasa typu obiektu docelowego
	 * @return obiekt zdeserializowany na podstawie danych z JSON'a.
	 */
	public <T> T fromJson(final String json, final Class<T> classOfT) {
		return gson.fromJson(json, classOfT);
	}

	/**
	 * Konwersja strumienia z JSON'em na wskazany typ obiektu.
	 * 
	 * @param <T>
	 *            Typ zwracanego obiektu, do którego dokonujemy konwersji.
	 * @param json
	 *            strumien zawierający dane.
	 * @param typeToken
	 *            typ obiektu, na który przeprowadzamy konwersję
	 * @param encoding
	 *            kodowanie znaków.
	 * @return zdekodowany strumień.
	 * @throws UnsupportedEncodingException
	 *             kodowanie znaków, które nie jest wspierane przez platformę -
	 *             podajemy UTF-8 i problem nie wystąpi.
	 */
	public <T> T fromJson(final InputStream json, final TypeToken<T> typeToken,
			final String encoding) throws UnsupportedEncodingException {
		T ret = gson.fromJson(new InputStreamReader(new BufferedInputStream(
				json), encoding), typeToken.getType());
		return ret;
	}

	/**
	 * Konwersja strumienia z JSON'em na wskazany typ obiektu.
	 * 
	 * @param <T>
	 *            Typ zwracanego obiektu, do którego dokonujemy konwersji.
	 * @param json
	 *            strumien zawierający dane.
	 * @param typeToken
	 *            typ obiektu, na który przeprowadzamy konwersję
	 * @param encoding
	 *            kodowanie znaków.
	 * @return zdekodowany strumień.
	 * @throws UnsupportedEncodingException
	 *             kodowanie znaków, które nie jest wspierane przez platformę -
	 *             podajemy UTF-8 i problem nie wystąpi.
	 */
	public <T> T fromJson(final InputStream json, final TypeToken<T> typeToken)
			throws UnsupportedEncodingException {
		return this.fromJson(json, typeToken, "UTF-8");
	}

	/**
	 * serializacja obiektu do JSON'a.
	 * 
	 * @param obj
	 *            obiekt, który ma być zserializowany.
	 * @return JSON reprezentujący obiekt.
	 */
	public String toJson(final Object obj) {
		return gson.toJson(obj);
	}

	/**
	 * 
	 * @param t
	 *            obiekt do serializacji.
	 * @param entityStream
	 *            strumień, do którego jest zapisany json reprezentujący obiekt.
	 * @throws IOException
	 *             problem z zapisem do strumienia.
	 */
	public void toJson(final Object t, final OutputStream entityStream)
			throws IOException {
		Writer a = new OutputStreamWriter(entityStream);
		gson.toJson(t, a);
		a.flush();
	}

}
