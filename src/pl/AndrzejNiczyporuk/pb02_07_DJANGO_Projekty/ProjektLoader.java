package pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty;

import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db.ProjektToDo;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db.ProjektContract;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db.ProjektDBOpenHelper;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db.ProjektContract.Projekt;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class ProjektLoader extends AsyncTaskLoader<ProjektToDo> {

	private ProjektDBOpenHelper dbHelper;
	private long noteId;

	public ProjektLoader(Context context, long noteId) {
		super(context);
		dbHelper = new ProjektDBOpenHelper(context);
		this.noteId = noteId;
	}

	@Override
	public ProjektToDo loadInBackground() {
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		String[] columns = new String[] { Projekt.COLUMN_NAME_ODDATA,
				Projekt.COLUMN_NAME_OPIS, Projekt.COLUMN_NAME_NAZWA, Projekt.COLUMN_NAME_DODATA, 
				Projekt.COLUMN_NAME_LINK , Projekt.COLUMN_NAME_STATUS , Projekt.COLUMN_NAME_WYKONAWCY };
		String selection = Projekt._ID + " = ?";
		String[] selectionArgs = new String[] { String.valueOf(noteId) };
		String groupBy = null;
		String having = null;
		String orderBy = null;
		Cursor cursor = null;
		try {
			cursor = db.query(ProjektContract.Projekt.TABLE_NAME, columns,
					selection, selectionArgs, groupBy, having, orderBy);
			boolean exists = cursor.moveToFirst();
			if (exists) {
				ProjektToDo ret = new ProjektToDo();
				ret.oddata = cursor.getString(cursor
						.getColumnIndex(Projekt.COLUMN_NAME_ODDATA));
				ret.opis = cursor.getString(cursor
						.getColumnIndex(Projekt.COLUMN_NAME_OPIS));
				ret.nazwa = cursor.getString(cursor
						.getColumnIndex(Projekt.COLUMN_NAME_NAZWA));
				ret.dodata = cursor.getString(cursor
						.getColumnIndex(Projekt.COLUMN_NAME_DODATA));
				ret.link = cursor.getString(cursor
						.getColumnIndex(Projekt.COLUMN_NAME_LINK));
				ret.status = cursor.getString(cursor
						.getColumnIndex(Projekt.COLUMN_NAME_STATUS));	
				ret.wykonawcy = cursor.getString(cursor
								.getColumnIndex(Projekt.COLUMN_NAME_WYKONAWCY));			
				
				
				return ret;
			} else {
				return null;
			}
		} finally {
			if (cursor != null) {
				cursor.close();
			}
			db.close();
		}
	}

	@Override
	protected void onStartLoading() {
		super.onStartLoading();
		forceLoad();
	}

	@Override
	public void onCanceled(ProjektToDo data) {
		super.onCanceled(data);
		dbHelper.close();
	}
	@Override
	protected void onReset() {
		super.onReset();
		dbHelper.close();
	}
}
