package pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty;

import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.R;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db.ProjektToDo;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db.ProjektDBOpenHelper;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db.ProjektContract.Projekt;
import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.ContentValues;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * A placeholder fragment containing a simple view.
 */
public class ProjektEditorFragment extends Fragment implements
		LoaderCallbacks<ProjektToDo> {
	private static final String NOTE_ID_KEY = "note_id_key";

	/**
	 * id loadera wczytującego pojedyńczy projekt .
	 */
	private static final int NOTE_LOADER = 0;

	private ProjektDBOpenHelper dbHelper;

	private TextView etNazwa;
	private TextView etOpis;
	private TextView etOddata;
	private TextView etDodata;
	private TextView etLink;
	private TextView etStatus;
	private TextView etWykonawcy;	    

	public interface ProjektEditorListener {
		void onBtnOkClick(String nazwa, String opis, String oddata, String dodata, String link, String status, String wykonawcy);
		
		//dodac reszte pol lub jason 

	}

	private ProjektEditorListener listener;

	public ProjektEditorFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_note_editor, container,
				false);
		View btnOk = rootView.findViewById(R.id.button1);
		etNazwa = (TextView) rootView.findViewById(R.id.etNazwa);
		etOpis = (TextView) rootView.findViewById(R.id.etOpis);
		etOddata=(TextView) rootView.findViewById(R.id.etOddata);
		etDodata=(TextView) rootView.findViewById(R.id.etDodata);
		etLink=(TextView) rootView.findViewById(R.id.etLink);
		etStatus=(TextView) rootView.findViewById(R.id.etStatus);
		etWykonawcy=(TextView) rootView.findViewById(R.id.etWykonawcy);	
		
		
		
		
		btnOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String opis = etOpis.getText().toString();
				String nazwa = etNazwa.getText().toString();
				String oddata = etOddata.getText().toString();
				String dodata = etDodata.getText().toString();
				String link = etLink.getText().toString();
				String status = etStatus.getText().toString();
				String wykonawcy = etWykonawcy.getText().toString();
				
				listener.onBtnOkClick(nazwa, opis,oddata,dodata,link,status,wykonawcy);


			}
		});
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (getArguments() != null && getArguments().containsKey(NOTE_ID_KEY)) {
			LoaderManager lm = getLoaderManager();
			lm.initLoader(NOTE_LOADER, null, this);
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.listener = (ProjektEditorListener) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		this.listener = null;
	}

	public static Fragment newInstance(long id) {
		// Przekaz parametr przez bundla
		Fragment ret = new ProjektEditorFragment();
		if (id>=0) // jeżeli id >=0 to znaczy że przekazanie argumnetu jak <0 to znaczy że utwórz nową instancję 
		{	
		Bundle args = new Bundle();
		args.putLong(NOTE_ID_KEY, id);
		ret.setArguments(args);
		}
		return ret;
	}

	@Override
	public Loader<ProjektToDo> onCreateLoader(int id, Bundle args) {
		long noteId = getArguments().getLong(NOTE_ID_KEY);
		return new ProjektLoader(getActivity().getApplicationContext(), noteId);
	}

	@Override
	public void onLoadFinished(Loader<ProjektToDo> loader, ProjektToDo data) {
		View view = getView();
		if (view != null) {
			TextView etNazwa = (TextView) view.findViewById(R.id.etNazwa);
			TextView etOpis = (TextView) view.findViewById(R.id.etOpis);
			TextView etOddata = (TextView) view.findViewById(R.id.etOddata);
			TextView etDodata = (TextView) view.findViewById(R.id.etDodata);
			TextView etLink = (TextView) view.findViewById(R.id.etLink);
			TextView etStatus = (TextView) view.findViewById(R.id.etStatus);
			TextView etWykonawcy = (TextView) view.findViewById(R.id.etWykonawcy);
			
			etOpis.setText(data.opis);
			etNazwa.setText(data.nazwa);
			etOddata.setText(data.oddata);
			etDodata.setText(data.dodata);
			etLink.setText(data.link);
			etStatus.setText(data.status);
			etWykonawcy.setText(data.wykonawcy);
		}

	}

	@Override
	public void onLoaderReset(Loader<ProjektToDo> loader) {
		View view = getView();
		if (view != null) {
			TextView etNazwa = (TextView) view.findViewById(R.id.etNazwa);
			TextView etOpis = (TextView) view.findViewById(R.id.etOpis);
			TextView etOddata = (TextView) view.findViewById(R.id.etOddata);
			TextView etDodata = (TextView) view.findViewById(R.id.etDodata);
			TextView etLink = (TextView) view.findViewById(R.id.etLink);
			TextView etStatus = (TextView) view.findViewById(R.id.etStatus);
			TextView etWykonawcy = (TextView) view.findViewById(R.id.etWykonawcy);
			
			etOpis.setText("");
			etNazwa.setText("");
			etOddata.setText("");
			etDodata.setText("");
			etLink.setText("");
			etStatus.setText("");
			etWykonawcy.setText("");
		}

	}

}