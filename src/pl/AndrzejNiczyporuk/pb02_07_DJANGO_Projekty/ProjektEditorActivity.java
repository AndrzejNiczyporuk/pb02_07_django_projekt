package pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty;

import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.R;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.ProjektEditorFragment.ProjektEditorListener;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db.ProjektContract;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

//extends ActionBarActivity
public class ProjektEditorActivity  extends Activity implements
		ProjektEditorListener {
	public static final String NOTE_ID_KEY = "note_id_key";
	private static final String TAG = ProjektEditorActivity.class.getName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_note_editor);
		if (savedInstanceState == null) {
			android.app.FragmentManager fm = getFragmentManager();
			android.app.FragmentTransaction fragmentTransaction = fm.beginTransaction();
			long id = getIntent().getLongExtra(NOTE_ID_KEY, -1);
			fragmentTransaction.add(R.id.fl_note_editor, ProjektEditorFragment.newInstance(id));
			fragmentTransaction.commit();
			
			
		}
	}

	@Override
	public void onBtnOkClick(String nazwa, String opis, String oddata, String dodata, String link, String status, String wykonawcy) {
		// TODO Auto-generated method stub
		// Log.w(TAG, "onBtnOKClick() - not implemented");
		//zapisz do listy  
		MainActivity.SaveToDb(nazwa, opis,oddata,dodata,link,status,wykonawcy);
		ProjektEditorActivity.this.getContentResolver().notifyChange(ProjektContract.NOTE_URI, null, false);
		finish();
		
	}

}
