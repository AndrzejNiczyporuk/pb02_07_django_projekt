package pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db;

import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db.ProjektContract.Projekt;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class ProjektDBOpenHelper extends SQLiteOpenHelper {

	// u�yteczne sta�e
		private static final String TEXT_TYPE = " TEXT";
		// tak naprawd� to b�dzie typ przylegaj�cy do numerycznego - uroda SQLite.
		private static final String DATE_TYPE = " DATETIME";
		private static final String COMMA_SEP = ",";
		// jesli zmieniamy schemat bazy - trzeba podbi� numerek, �eby zosta�a
		// wywo�ana metoda onUpgrade().
		private static final int DATABASE_VERSION = 1;
		private static final String DATABASE_NAME = "projekt.db";
	/**
	 * SQL tworzenia schematu bazy danych - archaicznie, ale tak wypada na
	 * pocz�tku - mo�na wspomaga� si� frameworkami, np.: ORMLite.
	 */
	private static final String SQL_CREATE_SCHEMA = "CREATE TABLE "
			+ Projekt.TABLE_NAME + " (" + Projekt._ID + " INTEGER PRIMARY KEY,"
			+ Projekt.COLUMN_NAME_NAZWA + TEXT_TYPE + COMMA_SEP
			+ Projekt.COLUMN_NAME_OPIS + TEXT_TYPE + COMMA_SEP
			+ Projekt.COLUMN_NAME_ODDATA + DATE_TYPE + COMMA_SEP
			+ Projekt.COLUMN_NAME_DODATA + DATE_TYPE + COMMA_SEP
			+ Projekt.COLUMN_NAME_LINK + TEXT_TYPE + COMMA_SEP
			+ Projekt.COLUMN_NAME_STATUS + TEXT_TYPE + COMMA_SEP
			+ Projekt.COLUMN_NAME_WYKONAWCY + TEXT_TYPE + COMMA_SEP  
			+ Projekt.COLUMN_NAME_SERVER_ID + TEXT_TYPE + " )";
	
	
	/** sql usuwania schematu */
	private static final String SQL_DROP_SCHEMA = "DROP TABLE IF EXISTS "
			+ Projekt.TABLE_NAME;

	public ProjektDBOpenHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
		// przy tworzeniu pobiera dane jasona 
		
		db.execSQL(SQL_CREATE_SCHEMA);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// tu normalnie trzeba by wysili� si� na jakies ALTER table i CREATE
		// nowych, je�li nie uzytkownik m�g� mie� warto�ciowe dane, to ich
		// usuni�cie przy aktualizacji jest s�abym pomys�em.
		db.execSQL(SQL_DROP_SCHEMA);
		onCreate(db);

	}

}
