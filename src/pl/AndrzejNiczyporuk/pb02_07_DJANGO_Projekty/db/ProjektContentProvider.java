package pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db;

import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db.ProjektContract.Projekt;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db.ProjektDBOpenHelper;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class ProjektContentProvider extends ContentProvider {
	
	public static final String AUTHORITY = "pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.provider";
    private ProjektDBOpenHelper dbHelper;
    
    
	@Override
	public boolean onCreate() {
		dbHelper = new ProjektDBOpenHelper(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		String groupBy = null;
		String having = null;
		Cursor cursor = db.query(Projekt.TABLE_NAME, projection, selection,
				selectionArgs, groupBy, having, sortOrder);
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		long id = db.insert(Projekt.TABLE_NAME, null, values);
		getContext().getContentResolver().notifyChange(
				ProjektContract.NOTE_URI, null, true); 
		Uri resultUri = ProjektContract.NOTE_URI.buildUpon()
				.appendPath(String.valueOf(id)).build();
		return resultUri;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		try {
			SQLiteDatabase db = dbHelper.getWritableDatabase();
			// Dla uproszczenia obsługuję tu tylko update konkretnego wiersza na
			// podstawie URI - w bardziej rozbudowanym przykładzie należy użyć
			// UriMatcher i sprawdzić do czego pasuje URI i selecion
			String path = uri.getLastPathSegment();

			String whereClause = Projekt._ID + " = ?";
			String[] whereArgs = new String[] { path };
			return db.update(Projekt.TABLE_NAME, values, whereClause, whereArgs);
		} finally {
			getContext().getContentResolver().notifyChange(uri, null, false);
		}
	}

}
