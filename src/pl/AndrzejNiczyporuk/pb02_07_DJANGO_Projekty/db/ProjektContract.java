package pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db;

import android.net.Uri;
import android.provider.BaseColumns;

public final class ProjektContract {
	
	// public static final Uri NOTE_URI =
		// Uri.parse("content://my_notepad/note");
		public static final Uri NOTE_URI = Uri
				.parse("content://pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.provider/"
						+ Projekt.TABLE_NAME);
		
	//public static final Uri NOTE_URI = Uri.parse("content://my_projekty/projekt");

	/**
	 * nie potrzeba instancji tej klasy
	 */
	private ProjektContract() {

	}

	public static abstract class Projekt implements BaseColumns {
		/** nazwa tabeli */
		public static final String TABLE_NAME = "projekt";

		/** nazwa kolumny - tytuł projketu  */
		public static final String COLUMN_NAME_NAZWA = "nazwa";
		/** nazwa kolumny - opis  */
		public static final String COLUMN_NAME_OPIS = "opis";
		/** nazwa kolumny - oddata */
		public static final String COLUMN_NAME_ODDATA = "oddata";
		
		/** nazwa kolumny - dodata */
		public static final String COLUMN_NAME_DODATA = "dodata";
		
		/** nazwa kolumny - link */
		public static final String COLUMN_NAME_LINK = "link";
		
		/** nazwa kolumny - status */
		public static final String COLUMN_NAME_STATUS = "status";
		
		/** nazwa kolumny - wykonawcy */
		public static final String COLUMN_NAME_WYKONAWCY  = "wykonawcy";
		
		/** nazwa kolumny - ID z serwera*/
		public static final String COLUMN_NAME_SERVER_ID = "server_id";


	}

}
