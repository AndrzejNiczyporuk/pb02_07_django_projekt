package pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.R;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.ProjektEditorFragment.ProjektEditorListener;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.ProjektyListAdapter.ViewHolder;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.ProjektyListFragment.NoteListListener;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db.ProjektContentProvider;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db.ProjektContract;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db.ProjektDBOpenHelper;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.db.ProjektContract.Projekt;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.sync.DownloadProjektCommand;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.sync.JSONConverter;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.sync.NotesResultWs;
import pl.AndrzejNiczyporuk.pb02_07_DJANGO_Projekty.sync.WsClient;
import android.app.Activity;
import android.app.ActionBar;
import android.content.ContentProvider;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.os.Build;

public class MainActivity extends FragmentActivity implements
		ProjektEditorListener, NoteListListener {
	private static final String TAG = MainActivity.class.getName();
	private static ProjektDBOpenHelper dbHelper;

	private interface onProjektClickHandler {
		void onProjketClick(long id);
	}

	private class OnProjektClickDualPaneHandler implements onProjektClickHandler {
		@Override
		public void onProjketClick(long id) {
			android.app.FragmentManager fm = getFragmentManager();
			android.app.FragmentTransaction fragmentTransaction = fm
					.beginTransaction();
			fragmentTransaction.replace(R.id.flEditorContainer,
					ProjektEditorFragment.newInstance(id));
			fragmentTransaction.commit();

		}
	}

	private class OnProjektClickOnePaneHandler implements onProjektClickHandler {

		@Override
		public void onProjketClick(long id) {
			Intent intent = new Intent(getApplicationContext(),
					ProjektEditorActivity.class);
			intent.putExtra(ProjektEditorActivity.NOTE_ID_KEY, id);
			startActivity(intent);
		}

	}

	private onProjektClickHandler onNoteclickHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		if (getResources().getBoolean(R.bool.has_two_panes)){
			onNoteclickHandler = new OnProjektClickDualPaneHandler();
		}
		else {
			onNoteclickHandler = new OnProjektClickOnePaneHandler();
		}

		if (savedInstanceState == null) {

			if (getResources().getBoolean(R.bool.has_two_panes)) {

				getFragmentManager().beginTransaction()
						.add(R.id.flEditorContainer, new ProjektEditorFragment())
						.commit();
			}

			
			getFragmentManager().beginTransaction()
					.add(R.id.flListContainer, new ProjektyListFragment()).commit();
		}

		this.dbHelper = new ProjektDBOpenHelper(getApplicationContext());
		
		
		/*Potrzebne do uruchamiania w UI zapisu do bazy 
		 * StrictMode.ThreadPolicy policy = new StrictMode.
		ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);*/
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public boolean isNetworkAvailable() {
	    ConnectivityManager cm = (ConnectivityManager) 
	      getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo networkInfo = cm.getActiveNetworkInfo();
	    // if no network is available networkInfo will be null
	    // otherwise check if we are connected
	    if (networkInfo != null && networkInfo.isConnected()) {
	        return true;
	    }
	    return false;
	} 
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		if (id == R.id.action_new_editor) {
			Intent intent = new Intent(getApplicationContext(),
					ProjektEditorActivity.class);
			intent.putExtra(ProjektEditorActivity.NOTE_ID_KEY, -1);
			startActivity(intent);	
		}
		
		if (id == R.id.synchronize) {
        // 
			Log.i(TAG, "Wywołano synchronizację ");
			final SyncResult syncResult = new SyncResult();
			//ProjektContentProvider provider = new ProjektContentProvider();
//			boolean performSync = DownloadProjektCommand.performSync(syncResult);
//			getContentResolver().get
			
			
	        WsClient wsClient = new WsClient();
			if (isNetworkAvailable()) 
			{	
	
	/* try {
//		NotesResultWs downloadNotesResult = wsClient.downloadNotes("http://192.168.139.1/jason/projekt-list/");
		//Log.d(TAG, "performSync() note: " + JSONConverter.getInstance().toJson(downloadNotesResult));
		 DownloadProjektCommand.performSync(this.getApplicationContext(), syncResult);
		 Log.i(TAG, "No i co widać ??");
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}*/
				
}
			new AsyncTask<Context, Void, Void>() {
				protected Void doInBackground(Context[] params) {
				try {
					boolean a= DownloadProjektCommand.performSync(params[0]);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
//					boolean performSync = DownloadProjektCommand.performSync(syncResult);
					return null;
				}
			
			}.execute(this.getApplicationContext());			
		}
		

		return super.onOptionsItemSelected(item);
	}

	public static void SaveToDb(final String nazwa, final String opis, final String oddata, final String dodata,  final String link, final String status, final String wykonawcy) {
		if (nazwa.length() > 0) {
			
		new AsyncTask<Void, Void, Void>() {
			protected Void doInBackground(Void[] params) {
				// przy zapisie uzywamy sta�ego formatu niezaleznie od
				// preferencji uzytkownika.
				// android.text.format.DateFormat mo�na u�y� do przy
				// wyswietlaniu daty uzytkownikowi.
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");

				SQLiteDatabase db = null;
				try {

					db = dbHelper.getWritableDatabase();
					ContentValues values = new ContentValues();
					values.put(Projekt.COLUMN_NAME_OPIS, opis);
					values.put(Projekt.COLUMN_NAME_NAZWA, nazwa);
					values.put(Projekt.COLUMN_NAME_ODDATA, oddata);
					values.put(Projekt.COLUMN_NAME_DODATA, dodata);
					values.put(Projekt.COLUMN_NAME_LINK, link);
					values.put(Projekt.COLUMN_NAME_STATUS, status);
					values.put(Projekt.COLUMN_NAME_WYKONAWCY, wykonawcy);
				//	values.put(Projekt.COLUMN_NAME_SERVER_ID, ID);
					db.insert(Projekt.TABLE_NAME, null, values);
					

				} finally {
					if (db != null) {
						db.close();
					}
				}

				return null;
			};
		}.execute();
		} else {
			// Toast.makeText(MainActivity.this, "Nie zapisano bo nie podano tytułu", Toast.LENGTH_LONG).show();
			Log.i("Main", "Nie zapisano bo nie podano nazwy");
		}

	}

	

	@Override
	public void onListClick(long id) {
		Log.w(TAG, "onNoteClick(); id: " + id);

		onNoteclickHandler.onProjketClick(id);

	}

	@Override
	public void onListLongClick(final long id) {
		// usunięcie z bazy pol title i context

		new AsyncTask<Void, Void, Void>() {
			protected Void doInBackground(Void[] params) {

				SQLiteDatabase db = null;
				try {

					db = dbHelper.getReadableDatabase();

					String whereClause = Projekt._ID + " = ?";
					String[] whereArgs = { Long.toString(id) };
					db.delete(ProjektContract.Projekt.TABLE_NAME, whereClause,
							whereArgs);

					// notyfikacja �e zmieniono dane
					MainActivity.this.getContentResolver().notifyChange(
							ProjektContract.NOTE_URI, null, false);
				} finally {
					if (db != null) {
						db.close();
					}
				}

				return null;
			};
		}.execute();

	}

	@Override
	public void onBtnOkClick(String nazwa, String opis, String oddata, String dodata, String link, String status, String wykonawcy) 
	{

			// Zapis do bazy
			// zapis do bazy pol title i context
				SaveToDb(nazwa, opis,oddata,dodata,link,status,wykonawcy);
				// notyfikacja że zmieniono dane
				MainActivity.this.getContentResolver().notifyChange(
						ProjektContract.NOTE_URI, null, false);
		}

}
